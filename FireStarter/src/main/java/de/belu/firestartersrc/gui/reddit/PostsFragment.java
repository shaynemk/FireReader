package de.belu.firestartersrc.gui.reddit;

/**
 * Created by skeller on 8/11/15.
 */

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.belu.firestartersrc.R;

/**
 * While this looks like a lot of code, all this class
 * actually does is load the posts in to the listview.
 *
 * @author Hathy
 */
public class PostsFragment extends Fragment {

    ListView postsList;
    ArrayAdapter<Post> adapter;
    Handler handler;

    String subreddit;
    List<Post> posts;
    PostsHolder postsHolder;

    public PostsFragment(){
        handler=new Handler();
        posts=new ArrayList<Post>();
    }

    public static Fragment newInstance(String subreddit){
        PostsFragment pf=new PostsFragment();
        pf.subreddit=subreddit;
        pf.postsHolder=new PostsHolder(pf.subreddit);
        Log.d("RedditDebug","PostsFragment.newInstance('" + subreddit + "')");
        return pf;
    }
    public static Fragment newInstance(){
        String subreddit = "askreddit";
        PostsFragment pf=new PostsFragment();
        pf.subreddit=subreddit;
        pf.postsHolder=new PostsHolder(pf.subreddit);
        Log.d("RedditDebug","PostsFragment.newInstance(DEFAULT/askreddit)");
        return pf;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //setRetainInstance(true);

        /*if (postsHolder == null) newInstance();
        initialize();*/

        View v=inflater.inflate(R.layout.redditfragment_items, container, false);
        postsList=(ListView) v.findViewById(R.id.posts_list);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialize();
    }

    private void initialize(){
        // This should run only once for the fragment as the
        // setRetainInstance(true) method has been called on
        // this fragment

        if (postsHolder == null) newInstance();

        if(posts.size()==0){

            // Must execute network tasks outside the UI
            // thread. So create a new thread.
            new Thread(){
                public void run(){
                    try {
                        posts.addAll(postsHolder.fetchPosts());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // UI elements should be accessed only in
                    // the primary thread, so we must use the
                    // handler here.

                    handler.post(new Runnable() {
                        public void run() {
                            createAdapter();
                        }
                    });

                    //new CustomTask().execute((Void[])null);
                }
            }.start();
        }else{
            createAdapter();
        }
    }

    /**
     * This method creates the adapter from the list of posts
     * , and assigns it to the list.
     */
    private void createAdapter(){

        // Make sure this fragment is still a part of the activity.
        if(getActivity()==null) return;

        //adapter = new ArrayAdapter<Post>(){};

        adapter= new ArrayAdapter<Post>(getActivity(), R.layout.redditfragment_items, posts){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                if(convertView==null){
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.redditfragment_items, null);
                }

                TextView postTitle = (TextView) convertView.findViewById(R.id.post_title);
                TextView postDetails = (TextView) convertView.findViewById(R.id.post_details);
                TextView postScore = (TextView) convertView.findViewById(R.id.post_score);

                postTitle.setText(posts.get(position).title);
                postDetails.setText(posts.get(position).getDetails());
                postScore.setText(posts.get(position).getScore());

                return convertView;
            }
        };
        postsList.setAdapter(adapter);
    }

}