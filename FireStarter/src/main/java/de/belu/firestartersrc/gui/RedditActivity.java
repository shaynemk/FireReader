package de.belu.firestartersrc.gui;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import de.belu.firestartersrc.gui.reddit.PostsFragment;

/**
 * Created by skeller on 8/11/15.
 */
public class RedditActivity extends Fragment {

    public RedditActivity () {
        Log.d("RedditDebug","Constructor");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("RedditDebug", "onCreate");
        PostsFragment.newInstance("askreddit");

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }
}
