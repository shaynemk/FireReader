package com.keller23.android.amazon.firereader;

import android.content.Intent;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.TextView;

public class MainActivityTest extends android.test.ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                MainActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    @SmallTest
    public void testLayoutExists() {
        assertNotNull(activity.findViewById(R.id.main_textView_home));
        TextView homepage = (TextView)activity.findViewById(R.id.main_textView_home);
        assertEquals(activity.getString(R.string.andr_homepage),homepage.getText().toString());
    }


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
