package com.keller23.android.amazon.firereader;

import android.content.Intent;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.TextView;

import com.keller23.android.amazon.firereader.R;

public class SubredditActivityTest extends android.test.ActivityUnitTestCase<SubredditActivity> {

    private SubredditActivity activity;

    public SubredditActivityTest() {
        super(SubredditActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                SubredditActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    @SmallTest
    public void testFragmentHolderExists() {
        assertNotNull(activity.findViewById(R.id.fragment_holder));
    }


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
