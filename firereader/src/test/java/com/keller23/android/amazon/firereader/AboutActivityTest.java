package com.keller23.android.amazon.firereader;

import android.os.Build;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class AboutActivityTest {
    private AboutActivity activity;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.setupActivity(AboutActivity.class);
    }

    @Test
    public void activityExists() throws Exception {
        assertNotNull("Activity is not instantiated", activity);
    }

    @Test
    public void checkText() throws Exception {
        TextView tvAbout = (TextView) activity.findViewById(R.id.tvAbout);
        assertNotNull("tvAbout doesn't exist.",tvAbout);
        assertTrue("About text is not equal to defined string.", activity.getString(R.string.about_text).equals(tvAbout.getText().toString()));
    }
}
