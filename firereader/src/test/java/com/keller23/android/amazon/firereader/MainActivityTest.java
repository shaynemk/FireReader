package com.keller23.android.amazon.firereader;

import android.content.Intent;
import android.os.Build;
import android.view.MenuItem;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static org.assertj.core.api.Assertions.assertThat;


@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class MainActivityTest {
    private MainActivity activity;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void activityExists() throws Exception {
        assertThat(activity).isNotNull();
    }
    
    @Test
    public void tvExistsAndHasCorrectText() throws Exception {
        TextView tvHomepage = (TextView) activity.findViewById(R.id.tvHomepage);
        assertThat(tvHomepage).isNotNull();
        assertThat(tvHomepage.getText().toString()).isEqualTo(activity.getString(R.string.andr_homepage));
    }

    // TODO: 8/19/15 Broke, throws NullPointerException. How are we supposed to test Menu/MenuItems?
    @Ignore
    @Test
     public void aboutActivityStartedOnClick() {
        //activity.findViewById(R.id.action_about).performClick();
        MenuItem item = new RoboMenuItem(R.id.action_about);
        //activity.onOptionsItemSelected(item);

        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        shadowActivity.clickMenuItem(R.id.action_about);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);

        //assertThat(shadowIntent.getComponent().getClassName()).isEqualTo(AboutActivity.class.getName());
    }

    /*@Test
    public void subredditActivityStartedOnClick() {
        activity.findViewById(R.id.action_subreddit).performClick();

        Intent expectedIntent = new Intent(activity, SubredditActivity.class);

        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Intent actualIntent = shadowActivity.getNextStartedActivity();

        assertTrue(actualIntent.filterEquals(expectedIntent));
    }

    @Test
    public void settingsActivityStartedOnClick() {
        activity.findViewById(R.id.action_settings).performClick();

        Intent expectedIntent = new Intent(activity, SettingsActivity.class);

        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Intent actualIntent = shadowActivity.getNextStartedActivity();

        assertTrue(actualIntent.filterEquals(expectedIntent));
    }*/
}