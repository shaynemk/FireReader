package com.keller23.android.amazon.firereader;

import android.os.Build;
import android.widget.ImageView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class SplashActivityTest {
    private SplashActivity activity;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.setupActivity(SplashActivity.class);
    }

    @Test
    public void activityExists() throws Exception {
        assertNotNull("Activity is not instantiated", activity);
    }

    @Test
    public void checkSplash() throws Exception {
        assertNotNull("splash_image doesn't exist.", activity.findViewById(R.id.splash_image));
        ImageView ivSplash = (ImageView) activity.findViewById(R.id.splash_image);
        assertTrue("",ivSplash.getDrawable().equals(activity.getDrawable(R.drawable.loading)));
    }
}
