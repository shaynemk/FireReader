package com.keller23.android.amazon.firereader;

import android.os.Build;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.robolectric.util.FragmentTestUtil.startFragment;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class PostsFragmentTest {
    //private PostsFragment activity;
    private PostsFragment fragment;

    @Before
    public void setUp() throws Exception {
        fragment = (PostsFragment) PostsFragment.newInstance();
        startFragment(fragment);
    }

    @Ignore
    @Test
    public void verifyFragmentExists() throws Exception {
        assertThat(fragment).isNotNull();
    }

    @Ignore
    @Test
    public void thisIsATestMethod() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
        //teardown
    }
}