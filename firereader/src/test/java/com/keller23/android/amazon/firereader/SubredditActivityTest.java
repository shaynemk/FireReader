package com.keller23.android.amazon.firereader;

import android.os.Build;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;


@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class SubredditActivityTest {
    private SubredditActivity activity;

    // TODO: 8/19/15 https://gitlab.com/shaynemk/FireReader/issues/1
    @Ignore
    @Before
    public void setup() throws Exception {
        //activity = Robolectric.setupActivity(SubredditActivity.class);
    }

    @Ignore
    @Test
    public void activityExists() throws Exception {
        assertThat(activity).isNotNull();
    }

    @Ignore
    @Test
    public void checkFragmentHolder() throws Exception {
        assertThat(activity.findViewById(R.id.fragment_holder)).isNotNull();
    }
}
