package com.keller23.android.amazon.firereader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true).build();
        Fabric.with(fabric);
        //Fabric.with(this, new Crashlytics());*/

        setContentView(R.layout.activity_splash);

        Thread timeout = new Thread(){
            public void run(){
                try{
                    sleep(2500);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent openMainActivity = new Intent("com.keller23.android.amazon.firereader.MAINACTIVITY");
                    startActivity(openMainActivity);
                    finish();
                }
            }
        };
        timeout.start();
    }
}
