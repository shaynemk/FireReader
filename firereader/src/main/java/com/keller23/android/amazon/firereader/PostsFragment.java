package com.keller23.android.amazon.firereader;


import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class PostsFragment extends ListFragment {

    ListView postsList;
    ArrayAdapter<Post> adapter;
    Handler handler;

    String subreddit;
    List<Post> posts;
    PostsHolder postsHolder;

    public PostsFragment(){
        handler=new Handler();
        posts=new ArrayList<Post>();
    }

    public static Fragment newInstance(String subreddit){
        PostsFragment pf=new PostsFragment();
        pf.subreddit=subreddit;
        pf.postsHolder=new PostsHolder(pf.subreddit);
        System.out.println("PostsFragment.newInstance('" + subreddit + "')");
        Log.d("RedditDebug", "PostsFragment.newInstance('" + subreddit + "')");
        return pf;
    }
    public static Fragment newInstance(){
        String default_subreddit = "askreddit";
        PostsFragment pf=new PostsFragment();
        pf.subreddit=default_subreddit;
        pf.postsHolder=new PostsHolder(pf.subreddit);
        System.out.println("PostsFragment.newInstance(DEFAULT/" + default_subreddit + ")");
        Log.d("RedditDebug","PostsFragment.newInstance(DEFAULT/" + default_subreddit + ")");
        return pf;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //setRetainInstance(true);

        //if (postsHolder == null) newInstance();
        initialize();

        View v=inflater.inflate(R.layout.fragment_reddit_items, container, false);
        postsList=(ListView) v.findViewById(R.id.posts_list);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialize();
    }

    private void initialize(){
        // This should run only once for the fragment as the
        // setRetainInstance(true) method has been called on
        // this fragment

        if (postsHolder == null) newInstance();

        if(posts.size()==0){

            // Must execute network tasks outside the UI
            // thread. So create a new thread.
            new Thread(){
                public void run(){
                    try {
                        posts.addAll(postsHolder.fetchPosts());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // UI elements should be accessed only in
                    // the primary thread, so we must use the
                    // handler here.

                    handler.post(new Runnable() {
                        public void run() {
                            createAdapter();
                        }
                    });

                    //new CustomTask().execute((Void[])null);
                }
            }.start();
        }else{
            createAdapter();
        }
    }

    /**
     * This method creates the adapter from the list of posts
     * , and assigns it to the list.
     */
    private void createAdapter(){

        // Make sure this fragment is still a part of the activity.
        if(getActivity()==null) return;
        if(postsList == null) newInstance();

        //adapter = new ArrayAdapter<Post>(){};

        adapter= new ArrayAdapter<Post>(getActivity(), R.layout.fragment_reddit_list, posts){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View iView = convertView;
                if(iView==null){
                    iView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_reddit_list, null);
                }

                TextView postTitle = (TextView) iView.findViewById(R.id.post_title);
                TextView postDetails = (TextView) iView.findViewById(R.id.post_details);
                TextView postScore = (TextView) iView.findViewById(R.id.post_score);

                postTitle.setText(posts.get(position).title);
                postDetails.setText(posts.get(position).getDetails());
                postScore.setText(posts.get(position).getScore());

                return iView;
            }
        };
        postsList.setAdapter(adapter);
    }
}
