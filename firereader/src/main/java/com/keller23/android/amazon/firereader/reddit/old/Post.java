package com.keller23.android.amazon.firereader.reddit.old;

public class Post {
    String subreddit;
    String title;
    String author;
    int points;
    int numComments;
    String permalink;
    String url;
    String domain;
    String id;

    String getDetails(){
        //String details="Poster: " + author + "; Comments: " + numComments;
        return "Poster: " + author + "; Comments: " + numComments;
    }

    String getTitle(){
        return title;
    }

    String getScore(){
        return Integer.toString(points);
    }
}
