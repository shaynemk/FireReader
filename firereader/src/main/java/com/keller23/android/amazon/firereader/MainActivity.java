package com.keller23.android.amazon.firereader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true).build();
        Fabric.with(fabric);
        //Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Opening Settings", Toast.LENGTH_SHORT).show();
                Intent settingsIntent = new Intent("com.keller23.android.amazon.firereader.SETTINGSACTIVITY");
                startActivity(settingsIntent);
                return true;
            case R.id.action_subreddit:
                Toast.makeText(MainActivity.this, "Subreddit Mode", Toast.LENGTH_SHORT).show();
                Intent subredditIntent = new Intent("com.keller23.android.amazon.firereader.SUBREDDITACTIVITY");
                startActivity(subredditIntent);
                return true;
            case R.id.action_about:
                Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                Intent aboutIntent = new Intent("com.keller23.android.amazon.firereader.ABOUTACTIVITY");
                startActivity(aboutIntent);
                return true;
            case R.id.action_crash:
                Toast.makeText(MainActivity.this, "Force Crash",Toast.LENGTH_SHORT).show();
                throw new RuntimeException("This is a forced crash.");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
    }
}
