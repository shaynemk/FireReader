package com.keller23.android.amazon.firereader;

import android.app.Activity;
import android.os.Bundle;

public class SubredditActivity extends Activity {

    //private EditText et_subreddit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subreddit);
        //et_subreddit = (EditText) findViewById(R.id.et_subreddit);
        addFragment();
    }

    private void addFragment(){
        //if (et_subreddit.getText().toString().equals("")) et_subreddit.setText("askreddit");
        //getFragmentManager().beginTransaction().add(R.id.activity_subreddit,PostsFragment.newInstance(/*et_subreddit.getText().toString()*/"askreddit")).commit();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_holder
                        , PostsFragment.newInstance("askreddit"))
                .commit();
    }
}
