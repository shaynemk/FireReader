package com.keller23.android.amazon.firereader;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

/**
 * Created by skeller on 8/13/15.
 */
public class SettingsFragment extends PreferenceFragment {

    private CheckBox cbDebug;
    private CheckBox cbSplash;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        Preference prefDeviceDetails = (Preference) findPreference("prefVirtualDeviceDetails");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        cbDebug = (CheckBox) view.findViewById(R.id.cbDebug);
        cbSplash = (CheckBox) view.findViewById(R.id.cbSplash);

        return view;
    }
}

/*

public class SettingsFragment_DEMO extends PreferenceFragment {

    private ListPreference mListPreference;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mListPreference = (ListPreference)  getPreferenceManager().findPreference("preference_key");
        mListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                // insert custom code
            }
        }

        return inflater.inflate(R.layout.activity_settings, container, false);
    }
}*/
